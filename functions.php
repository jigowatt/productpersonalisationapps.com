<?php

/**
 * Theme version number
 *
 * @NOTE: Update in style.scss also!
 */
$theme_version = '0.0.1';

// Toggle/Force development mode
define( 'IS_DEV', false );

/**
 * Detect if we're on a staging site based on URL
 */
$staging = (
  in_array( $_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1')) ||
  strpos($_SERVER['SERVER_NAME'], '.staging.') ||
  strpos($_SERVER['SERVER_NAME'], '.wpengine.') ||
  strpos($_SERVER['SERVER_NAME'], '.cloudwaysapps.')
) ? true : false ;
define( 'IS_STAGING', $staging);

/**
 * Alter theme version for dev/staging sites (for cache busting assets)
 */
$theme_version = (IS_DEV || IS_STAGING) ? $theme_version.'--dev-'.date('U') : $theme_version;
define( 'THEME_VERSION', $theme_version );


/**
 * Some helpful vars & constants
 */
$template_url  = get_template_directory_uri();                      // store the theme URL
define( 'THEME_PATH', get_template_directory());                   	// store the theme server path
define( 'PATH_THEME', $template_url . '/' );                        //theme root URL
define( 'PATH_TEMPLATES', '_templates/' );                          //theme templates
define( 'PATH_PARTIALS', '_templates/partials/' );                  //theme template partials
define( 'PATH_IMG', $template_url . '/dist/img/' );                 //theme images dir
define( 'PATH_JS', $template_url . '/dist/js/' );                   //theme javascript dir
define( 'PATH_PHP', THEME_PATH . '/_functional/' );             	//theme php dir


/**
 * Includes
 */
include_once( PATH_PHP . 'theme-setup.php' );           //stuff to setup on every project
include_once( PATH_PHP . 'woocommerce.php' );           //woocommerce functionality and overrides


/**
 * Register menus
 */
register_nav_menus( array(
	'main_menu' => 'Menu in the header of the site',
	'footer_menu_1' => 'Footer links 1',
	'footer_menu_2' => 'Footer links 2'
) );


/**
 * Register widget areas
 */
register_sidebar( array(
	'name'          => __( 'Main sidebar', 'namespace' ),
	'id'            => 'main-sidebar',
	'description'   => '',
	'class'         => '',
	'before_widget' => '<section id="%1$s" class="widget %2$s">',
	'after_widget'  => '</section>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

/**
 * Change button text
 */
add_filter( 'add_to_cart_text', 'woo_custom_product_add_to_cart_text', 9999 );            // < 2.1
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text', 9999 );  // 2.1 +

function woo_custom_product_add_to_cart_text() {
  return __( 'View App', 'woocommerce' );
}

add_action( 'woocommerce_after_single_product_summary', 'jw_chat_option', 10 );

function jw_chat_option() {
echo '<a class="button ask_btn" href="https://secure.livechatinc.com/licence/7158431/v2/open_chat.cgi?groups=0">Ask Questions?</a>';
}


/**
 * Stand alone product (ID 2753)
 * https://productpersonalisationapps.com/product/html-post-message-api/
 */
add_action('init', 'load_stand_alone_product_actions');
function load_stand_alone_product_actions(){
  add_action( 'woocommerce_before_main_content', 'ppa_stand_alone_product_thingy', 0 );
  add_action( 'woocommerce_after_shop_loop_item', 'ppa_add_single_product_loop_button_for_stand_alone_thingy' );
}
function ppa_stand_alone_product_thingy()
{
	global $post;
  if ( $post->ID == 2753 ) {

  	if ( ! is_product() ) return;

  	$product = wc_get_product( $post );

		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar' );
		remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
		add_action( 'woocommerce_single_product_summary', 'ppa_stand_alone_product_thingy_style' );
	}
}
function ppa_stand_alone_product_thingy_style()
{
  ?>
   <iframe id="ppa_stand_alone" src="https://developers-v2.custom-gateway.net/iframe-examples/text-area-messages.html " style="border: 0px;" width="100%" height="600"></iframe>

   <style type="text/css">
     #ppa_stand_alone iframe{ border: 0px; width: 100%; };
     #ppa_stand_alone{ line-height: 0px;  };
     #product-2753 form.cart{ display: none };
   </style>
  <?php
}
function ppa_add_single_product_loop_button_for_stand_alone_thingy(){
  global $post;
  if($post->ID == 2753)
    echo '<a href="'.get_the_permalink().'" class="button product_type_ec3d" rel="nofollow">View App</a>';
}
