<?php

/**
 * Add Theme Support
 */
add_action( 'after_setup_theme', 'namespace_theme_supports' );
function namespace_theme_supports()
{
	add_theme_support( 'post-thumbnails' ); //thumbnails (featured image)
	add_theme_support( 'menus' );           //custom menus
	add_theme_support( 'html5' );           //html5
}


/**
 * Add critical scripts & styles
 */
function namespace_critical_path_css_then_async_main() {
	echo '<style>';
	include TEMPLATEPATH.'/critical.css';
	echo '</style>';

	?>
	<script>
		function loadCSS( href, before, media ){
			"use strict";
			var ss = window.document.createElement( "link" );
			var ref = before || window.document.getElementsByTagName( "script" )[ 0 ];
			var sheets = window.document.styleSheets;
			ss.rel = "stylesheet";
			ss.href = href;
			ss.media = "only x";
			ref.parentNode.insertBefore( ss, ref );
			function toggleMedia(){
				var defined;
				for( var i = 0; i < sheets.length; i++ ){
					if( sheets[ i ].href && sheets[ i ].href.indexOf( href ) > -1 ){
						defined = true;
					}
				}
				if( defined ){
					ss.media = media || "all";
				}
				else {
					setTimeout( toggleMedia );
				}
			}
			toggleMedia();
			return ss;
		}
		loadCSS('<?php echo PATH_THEME; ?>style.css?v=<?php echo THEME_VERSION; ?>');
	</script>
	<noscript>
		<link rel="stylesheet" href="<?php echo PATH_THEME; ?>style.css?v=<?php echo THEME_VERSION; ?>">
	</noscript>
	<?php
}
add_action('wp_head', 'namespace_critical_path_css_then_async_main');


/**
 * Add page/post slug to <body> "class" attribute
 */
add_filter( 'body_class', 'namespace_add_body_class' );
function namespace_add_body_class( $classes )
{
	global $post;

	if ( isset( $post ) ) {
		$classes[] = $post->post_type . ' ' . $post->post_name;
	}

	$blogname = get_bloginfo( 'name' );

	$classes[] = sanitize_title( $blogname );

	return $classes;
}


/**
 * Load Full Scripts and Styles
 *
 * Note: IE specific imports can found further down this file
 */
add_action( 'get_footer', 'namespace_enqueue_all_the_things' );
function namespace_enqueue_all_the_things()
{

	// enable some PHP values to be available to our JavaScript
	$phpVars = array(
		'js' => PATH_JS,
		'img' => PATH_IMG,
		'uploads' => site_url()."/wp-content/uploads/"
	);

	// select2
	wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css' );
	wp_register_script( 'select2', "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js", array( 'jquery' ), null, $in_footer = false );
	
	// Main theme script
	wp_register_script( 'theme', PATH_JS.'theme.js', array( 'jquery', 'select2' ), $ver = THEME_VERSION, $in_footer = true );
	wp_localize_script( 'theme', 'phpVars', $phpVars );
	wp_enqueue_script( ['select2', 'theme']);
	
}


/**
 * Dashboard theme version number display
 */
if (!function_exists('namespace_output_theme_version_in_dashboard')){
	add_filter( 'update_right_now_text', 'namespace_output_theme_version_in_dashboard' );
	function namespace_output_theme_version_in_dashboard( $content ){
		$content = str_replace('%2$s', '%2$s v'.THEME_VERSION, $content);
		return $content;
	}
}


/**
 * Add Advanced Custom Fields Options page when plugin is active
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


/**
 * Remove wordpress logo menubar node
 */
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'wp-logo' );
}


/**
 * Remove dashboard widgets
 */
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
function remove_dashboard_widgets() {
    global $wp_meta_boxes;

    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
 
}
 