<?php

/**
 * Register widget areas
 */
add_action( 'widgets_init', 'namespace_widgets_init' );
function namespace_widgets_init()
{
	register_sidebar( array(
		'name'          => __( 'Main sidebar', 'namespace' ),
		'id'            => 'main-sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>'
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 1', 'namespace' ),
		'id'            => 'footer-widgets-1',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 2', 'namespace' ),
		'id'            => 'footer-widgets-2',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );
}