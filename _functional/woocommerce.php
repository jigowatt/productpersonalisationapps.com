<?php

/**
 * Adds support for WooCommerce functionality
 */
add_action( 'after_setup_theme', 'namespace_woocommerce_support' );
function namespace_woocommerce_support()
{
  add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

/**
 * Default loop columns on product archives
 * @return integer products per row
 */
add_filter( 'loop_shop_columns', 'namespace_loop_columns' );
if(!function_exists('namespace_loop_columns')){
    function namespace_loop_columns() {
        return 4; /* 4 products per row */
    }
}

/**
  * Wrap woo in custom contain
  */
add_action('woocommerce_before_main_content', 'jigowatt_custom_woo_wrapper_start');
function jigowatt_custom_woo_wrapper_start() {
	echo '<div class="contain">
  				<div id="content" class="row" >';
}

add_action('woocommerce_after_main_content', 'jigowatt_custom_woo_wrapper_end');
function jigowatt_custom_woo_wrapper_end() {
		echo '</div><!-- Close Container -->
	     </div><!-- Close Main-BG -->';
}

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
