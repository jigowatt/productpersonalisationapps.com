<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0" />

	<!-- page title -->
	<title><?php wp_title() ?></title>

	<!-- iOS -->
	<?php if(file_exists(THEME_PATH.'/dis/img/apple-touch-icon-precomposed.png')) : ?>
	<link rel="apple-touch-icon-precomposed" href="<?php echo PATH_IMG ?>apple-touch-icon-precomposed.png">
	<?php endif; ?>

	<!-- favicon -->
	<?php if(file_exists(THEME_PATH.'/dis/img/favicon.png')) : ?>
	<link rel="shortcut icon" href="<?php echo PATH_IMG ?>favicon.png" type="image/png" />
	<?php endif; ?>

	<?php wp_head(); ?>


</head>

<body <?php body_class('no-js') ?>>

	<!-- let CSS know JS is enabled -->
	<!-- inlined for performance -->
	<script>document.body.className = document.body.className.replace("no-js","has-js");</script>

	<!-- keep nav separate from header and content for "off-screen" mobile navigation -->
	<nav id="main-nav" class="main-nav">
		<a class="close menu-toggle" href="#main-content">Close</a>

		<?php
			$defaults = array(
				'theme_location'  => 'main_menu',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'menu menu--main',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $defaults );
		?>
	</nav>

	<!-- Branding bar -->
	<header class="main-header">

		<!-- logo -->
		<a class="logo" href="<?php echo get_site_url(); ?>">
			<?php if(file_exists(THEME_PATH.'/dis/img/logo.svg')) : ?>
				<img src="<?php echo THEME_PATH.'/dis/img/logo.svg'; ?>" width="280" alt="<?php echo get_bloginfo('name'); ?>">
			<?php else : 
				echo get_bloginfo('name'); ?>		
			<?php endif; ?>
		</a>
		
		<!-- menu toggle link -->
		<a href="#main-nav" class="menu-toggle"><?php _e('Menu', 'namespace'); ?></a>

	</header>

	<!-- Main content -->
	<div id="main-content" class="page-wrapper">