<?php

$prev_post = get_adjacent_post(false, '', true);
$next_post = get_adjacent_post(false, '', false);


if(!empty($prev_post) || !empty($next_post)):
?>

<div class="pagination">
	<?php if(!empty($prev_post)) : ?>
	<div class="prev-post"> 
		<span class="next-prev">Previous post:</span>
		<a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo $prev_post->post_title ?>"><?php
			echo $prev_post->post_title; ?>
		</a>
	</div>
	<?php endif; ?>

	<?php if(!empty($next_post)) : ?>
	<div class="next-post">
		<span class="next-prev">Next post:</span>
		<a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo $next_post->post_title ?>"><?php
			echo $next_post->post_title; ?>
		</a>
	</div>
	<?php endif; ?>
</div>

<?php
endif;
?>