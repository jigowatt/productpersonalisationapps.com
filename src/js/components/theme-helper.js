/**!
 * THEME HELPER OBJECT
 * This is useful for storing theme-wide util stuff
 */

// helper object to store it all
window.helper = {

	// kick off data grab
	init : function(){
		helper.viewport.get();
		helper.breakpoint.get();
	},

	// viewport info 
	viewport : {
		width  : 0,
		height : 0,
		get    : function(){
			helper.viewport.width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
			helper.viewport.height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		}
	},

	// breakpoint info (compliments mixins.scss breakpoints)
	breakpoint : {
		mobile  : false,
		tablet  : false,
		desktop : false,
		wide    : false,
		get     : function(){
			helper.breakpoint.mobile  = ( helper.viewport.width <= 375 ) ? true : false;
			helper.breakpoint.tablet  = ( helper.viewport.width >= 376 && helper.viewport.width <= 740 ) ? true : false;
			helper.breakpoint.desktop = ( helper.viewport.width >= 741 && helper.viewport.width <= 1299 ) ? true : false;
			helper.breakpoint.wide    = ( helper.viewport.width >= 1300 ) ? true : false;
		}
	},

	// console logging
	log : function(message) {
		if( window.console && console.log ) {
			console.log(message);
		}
	},
	warn : function(message) {
		if( window.console && console.warn ) {
			console.warn(message);
		}
	},
	info : function(message) {
		if( window.console && console.info ) {
			console.info(message);
		}
	},
	dir : function(thing) {
		if( window.console && console.dir ) {
			console.dir(thing);
		}
	}
};


// events
if(window.jQuery){
    jQuery(window).resize( function(){ window.helper.init(); } );
    jQuery('document').ready( function(){ window.helper.init(); } );
} else {
	window.addEventListener( 'resize', function(){ helper.init(); } );
	document.addEventListener( "DOMContentLoaded", function(){ helper.init(); } );
}