import helper from './components/theme-helper.js'; // theme helper object

(($, helper) => {
    
    // add select2 to all <select> elements
    if(typeof $.select2 === "function"){
        $(document).ready( () => { $('select').select2(); } );
    }
    
})(jQuery, helper);