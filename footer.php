	</div> <!-- Page Wrapper -->

	<footer class="main-footer">

		<?php if ( ! dynamic_sidebar( 'footer-widgets-1' ) ) : ?>

			<nav class="menu--footer">
				<?php
					$defaults = array(
						'theme_location'  => 'footer_menu',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu menu--footer',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);

					wp_nav_menu( $defaults );
				?>
			</nav>

		<?php endif; ?>

		<?php if ( ! dynamic_sidebar( 'footer-widgets-2' ) ) : ?>

			<nav class="menu--footer-2">
				<?php
					$defaults = array(
						'theme_location'  => 'footer_menu_2',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu menu--footer-2',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);

					wp_nav_menu( $defaults );
				?>
			</nav>

		<?php endif; ?>

		<div class="credit">Website by <?php if(is_front_page()) { echo '<a href="https://jigowatt.co.uk" target="new">Jigowatt</a>'; } else { echo 'Jigowatt'; } ?></div>
			
	</footer>

	<?php wp_footer() ?>

</body>
</html>